/**
 * Created by vurf on 03.12.2016.
 */

$( document ).ready(function() {

    console.log( "ready!" );

    $('#search').keypress(function (e) {
        if (e.which == 13) {
            var searchTerm = $("#search").val();
            console.log(searchTerm);
            window.location = "/search?text=" + searchTerm;
            return false;
        }
    });

});

