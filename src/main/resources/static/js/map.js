/**
 * Created by vurf on 17.10.16.
 * see https://github.com/Envek/Amestris/blob/master/script.js
 */

// chuk_ao
// nene_ao
// hant_ao
// yama_ao
//
// evre_ao //еврейская автономная область
//
// kamc_cr //камчатский край
// alta_cr //алтайский край
// kras_cr //красноярский край
// prim_cr //приморский край
// haba_cr //хабаровский
// zaba_cr //забайкальский
// krsd_cr //краснодарский
// stav_cr //ставропольский
// perm_cr //пермский
//
// saha_rp //республика Саха
// bury_rp //Бурятия
// tiva_rp //Тыва
// haka_rp //Хакасия
// komi_rp //Коми
// kare_rp //Карелия
// bash_rp //Башкортостан
// tata_rp //Татарстан
// chuv_rp //Чувашская
// mari_rp //Марий Эл
// mord_rp //Мордовия
// udmu_rp //Удмуртская
// chec_rp //Чеченская
// dage_rp //Дагестанская
// kalm_rp //Калмыкия
// ingu_rp //Ингушетия
// oset_rp //Северная Осетия-Алания
// kaba_rp //Кабардино-Балкарская Республика
// cher_rp //Карачаево-Черкесская Республика (Черкесск)
// adig_rp //Республика Адыгея (Майкоп)
// krim_rp //Республика Крым (Симферополь)
//
// maga_ob //Магаданская область
// saha_ob //Сахалинская область
// amur_ob //Амурская область
// irku_ob //Иркутская область
// keme_ob //Кемеровская область
// novo_ob //Новосибирская область
// toms_ob //Томская область
// omsk_ob //Омская область
// tyme_ob //Тюменская область
// kurg_ob //Курганская область
// sver_ob //Свердловская область
// chel_ob //Челябинская область
// orbg_ob //Оренбургская область
// sama_ob //Самарская область
// srtv_ob //Саратовская область
// volg_ob //Волгоградская область
// astr_ob //Астраханская область
// rstv_ob //Ростовская область
// ulya_ob //Ульяновская область
// penz_ob //Пензенская область
// tamb_ob //Тамбовская область
// voro_ob //Воронежская область
// belg_ob //Белгородская область
// kurs_ob //Курская область
// brya_ob //Брянская область
// smol_ob //Смоленская область
// psko_ob //Псковская область
// leni_ob //Ленинградская область
// murm_ob //Мурманская область
// novg_ob //Новгородская область
// volo_ob //Вологодская область
// arhg_ob //Архангельская область
// kiro_ob //Кировская область
// kost_ob //Костромская область
// yaro_ob //Ярославская область
// ivan_ob //Ивановская область
// nige_ob //Нижегородская область
// tver_ob //Тверская область
// mosc_ob //Московская область
// kalu_ob //Калужская область
// orlo_ob //Орловская область
// tuls_ob //Тульская область
// lipe_ob //Липецкая область
// ryaz_ob //Рязанская область
// vlad_ob //Владимирская область

// Определим свои функции добавления/удаления класса, так как те, что в jQuery не работают для SVG
jQuery.fn.myAddClass = function (classTitle) {
    return this.each(function() {
        var oldClass = jQuery(this).attr("class");
        oldClass = oldClass ? oldClass : '';
        jQuery(this).attr("class", (oldClass+" "+classTitle).trim());
    });
}

jQuery.fn.myRemoveClass = function (classTitle) {
    return this.each(function() {
        var oldClassString = ' '+jQuery(this).attr("class")+' ';
        var newClassString = oldClassString.replace(new RegExp(' '+classTitle+' ','g'), ' ').trim()
        if (!newClassString)
            jQuery(this).removeAttr("class");
        else
            jQuery(this).attr("class", newClassString);
    });
}

jQuery(document).ready(function ($) {

    // Получаем доступ к SVG DOM
    var svgobject = document.getElementById('imap');
    svgobject.addEventListener("load",function()
    {
        var svgDoc = svgobject.contentDocument;

        // svgDoc.querySelector(".st2").onclick = function ()
        // {
        //     alert("Центральный ФО");
        //     console.log("Центральный ФО");
        // };

        svgDoc.querySelector(".chuk_ao").onclick = function ()
        {
            alert("Чукотский автономный округ");
            console.log("Чукотский автономный округ");
        };

        svgDoc.querySelector(".nene_ao").onclick = function ()
        {
            alert("Ненецкий автономный округ");
            console.log("Ненецкий автономный округ");
        };

        svgDoc.querySelector(".hant_ao").onclick = function ()
        {
            alert("Ханты-Мансийский автономный округ");
            console.log("Ханты-Мансийский автономный округ");
        };

        svgDoc.querySelector(".yama_ao").onclick = function ()
        {
            alert("Ямало-Ненецкий автономный округ");
            console.log("Ямало-Ненецкий автономный округ");
        };

        svgDoc.querySelector(".evre_ao").onclick = function ()
        {
            alert("Еврейская автономная область");
            console.log("Еврейская автономная область");
        };

        svgDoc.querySelector(".kamc_cr").onclick = function ()
        {
            alert("Камчатский край");
            console.log("Камчатский край");
        };

    }, false);
});