/**
 * Created by vurf on 22.11.2016.
 */
$(document).ready(function() {
    changePageAndSize();
});

function changePageAndSize() {
    $('#pageSizeSelect').change(function(evt) {
        window.location.replace("/objects?pageSize=" + this.value + "&page=1");
    });
}