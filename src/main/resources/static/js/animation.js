Array.prototype.random = function()
{
    return this[Math.floor(Math.random() * (this.length + 1))];
}

var animation = (function()
{
    var color = ["red", "blue", "green", "black", "pink", "gray"];
    var boxes = ["1","2","3","4","5","6","7","8","9","10","11","12"];

    var currentAnim = "anim1";
    var randomBox;
    var min = 1;
    var max = 13;

    this.objects = [];

    function random()
    {
        return Math.floor(Math.random() * (max - min + 1) + min)
    }

    this.startImg = function()
    {
        var length = boxes.length;
        for (var i = 0; i < length; i++)
        {
            this.setImage(i);
        }
    }

    this.setBoxes = function()
    {
        var length = boxes.length;
        for (num = 0; num < length; num++)
        {
            var box = document.getElementById(boxes[num]);
            box.onclick = function()
            {
                location.href = this.link;
            }
            setImage(box);
            boxes[num] = box;
        }
    }

    function setImage(box)
    {
        var obj = getObject();
        box.style.position = "static";
        box.classList.remove("anim1");
        box.style.background = "url("+ obj.image +") center";
        box.style.backgroundSize = "cover";
        box.style.position = "relative";
        box.children[0].innerHTML = obj.name;
        box.link = obj.link;
        box.offsetWidth;
        box.classList.add("anim1");
        //style.backgroundImage = "url(http://lorempixel.com/"+width+"/"+ height+"/)";
        console.log("222222");
    }


    function getObject()
    {
        var obj = this.objects.shift();
        if (obj == undefined)
        {
            getObjects(this);
            obj = this.objects.shift();
        }

        return obj;
    }

    function getLink()
    {
        var link = this.links.shift();
        return link;
    }

    this.changePicture = function()
    {
        var box = boxes.random();
        setImage(box);
    }

    return this;
})();

var mutext = false;
function getObjects(animation)
{
    function getJSON( url, callback) {

        return jQuery.ajax({
            type: "GET",
            url: url,
            success: callback,
            dataType: "json",
            async: false,
            //error: ...
        });
    }

    console.log('get');
    getJSON("/objects/generator",function (result) {
        $.each (result,function (i,field) {
            animation.objects.push(field);
        })
    });
}

document.ready = function()
{
    animation.setBoxes();

    setInterval(function(){
        animation.changePicture()
    }, 5000);
    //$.get("http://194.67.197.51:8080/objects");
};
// document.ready = function(){
//  $("#1").mouseover(function(){
//      console.log("Hello");
//  });
// };