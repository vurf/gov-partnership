google.charts.load("current", {language: 'ru'});
google.charts.setOnLoadCallback(drawCharts);

function drawCharts()
{
	var colorPallette = chartsOptions.colors;
	var NumberOfLegendColoumns = 2;
	
	var data = chartsOptions.data;
	var size = data.length;
	data[0].push({role:'style'})
	for (var i = 1; i < size; i++)
	{
		data[i].push(colorPallette[i-1]);
	}
	
	data = new google.visualization.arrayToDataTable(data);
	var view = new google.visualization.DataView(data);
	view.setColumns([0, 1,
		{ calc: "stringify",
		 sourceColumn: 1,
		 type: "string",
		 role: "annotation",
		},
	2]);
	
	var chart1 = new google.visualization.ChartWrapper({
		chartType: 'ColumnChart',
		dataTable: view,
		options: {
			//title: 'Распределение объектов ГЧП в социальной сфере по отраслям реализации:',
			bar: {groupWidth: "60%"},
			legend: {position: 'none'},
			width: 486,
			height: 234,
			colors: colorPallette,
			chartArea: { left: 20, top: 20, bottom: 20, right: 20},
			hAxis: {textPosition: 'none'}
		},
		containerId: 'chart1'
	});
		
	var chart2 = new google.visualization.ChartWrapper({
		chartType: 'ColumnChart',
		dataTable: view,
		options: {
			//title: 'Распределение объектов ГЧП в социальной сфере по объёмам инвестиции и отраслям реализации:',
			bar: {groupWidth: "60%"},
			legend: {position: 'none'},
			width: 486,
			height: 235,
			colors: colorPallette,
			chartArea: { left: 20, top: 20, bottom: 20, right: 20},
			hAxis: {textPosition: 'none'}
		},
		containerId: 'chart2'
	});
	
	function incInRing(array, num)
	{
		return (++num >= array.length)? 0 : num;
	}
	
	var chart_1, chart_2;
	google.visualization.events.addOneTimeListener(chart2, 'ready', function ()
	{
		chart_2 = chart2.getChart();
	});
	
	google.visualization.events.addOneTimeListener(chart1, 'ready', function ()
	{
		var legend = document.getElementById('chart_legend');
		var rows = [];
		var rowsPos = 0;
		
		chart_1 = chart1.getChart();
		var dataTable = chart1.getDataTable();
	
		var length = dataTable.getNumberOfRows();
		for (var i = 0; i < length; i += NumberOfLegendColoumns)
		{
			var element = document.createElement('tr');
			rows.push(element);
			legend.appendChild(element);
		}
		for (var i = 0; i < length; i++)
		{
			addLegendMarker({
				index: i,
				marker: 'legend-marker-color-rect',
				color: colorPallette[i],
				label: dataTable.getValue(i, 0)
			});
		}

		var markers = legend.getElementsByTagName('td');
		Array.prototype.forEach.call(markers, function(marker)
		{
			while (marker.tagName.toLowerCase() !== 'td')
			{
				marker = marker.parentNode;
			}
			
			marker.addEventListener('click', function (e)
			{
				var columnIndex = parseInt(this.getAttribute('data-columnIndex'));
				if (chart_1) chart_1.setSelection([{row: columnIndex}]);
				if (chart_2) chart_2.setSelection([{row: columnIndex}]);
			}, false);

		});
		function addLegendMarker(markerProps)
		{
			var legendMarker = document.getElementById('template-legend-marker').innerHTML;
			for (var handle in markerProps)
			{
				if (markerProps.hasOwnProperty(handle))
				{
					legendMarker = legendMarker.replace('{' + handle + '}', markerProps[handle]);
				}
			}
			rows[rowsPos].insertAdjacentHTML('beforeEnd', legendMarker);
			rowsPos = incInRing(rows, rowsPos);
		}
	});
	
	chart1.draw();
	chart2.draw();
	
	var pchart_1, pchart_2, pchart_3;
	
	var pchart1 = new google.visualization.ChartWrapper({
		chartType: 'PieChart',
		dataTable: new google.visualization.arrayToDataTable(chartsOptions.data_p1),
		options: {
			//title: 'Соотношение реализованных и не реализованных объектов:',
			width: chartsOptions.pwidth,
			legend: {position: 'none'},
			colors: [colorPallette[5], colorPallette[6]],
			chartArea: {width: '90%', height: '90%' },
			legendId: 'chart_legend_p1'
		},
		containerId: 'piechart1'
	});
	
	google.visualization.events.addOneTimeListener(pchart1, 'ready', onPchartReady_p1);
	
	pchart1.draw();
	
	var pchart2 = new google.visualization.ChartWrapper({
		chartType: 'PieChart',
		dataTable: new google.visualization.arrayToDataTable(chartsOptions.data_p2),
		options: {
			//title: 'Распределение по уровням:',
			width: chartsOptions.pwidth,
			legend: {position: 'none'},
			colors: [colorPallette[6], colorPallette[5]],
			pieHole: 0.4,
			chartArea: {width: '90%', height: '90%' },
			legendId: 'chart_legend_p2'
		},
		containerId: 'piechart2'
	});
	
	google.visualization.events.addOneTimeListener(pchart2, 'ready', onPchartReady_p2);
	
	pchart2.draw();
	
	var pchart3 = new google.visualization.ChartWrapper({
		chartType: 'PieChart',
		dataTable: new google.visualization.arrayToDataTable(chartsOptions.data_p3),
		options: {
			//title: 'Доля региона по объёму инвестиций:',
			width: chartsOptions.pwidth,
			legend: {position: 'none'},
			colors: [colorPallette[5], colorPallette[6]],
			chartArea: {width: '90%', height: '90%' },
			legendId: 'chart_legend_p3'
		},
		containerId: 'piechart3'
	});
	
	google.visualization.events.addOneTimeListener(pchart3, 'ready', onPchartReady_p3);
	
	pchart3.draw();
	
	function onPchartReady_p1()
	{
		onPchartReady(pchart1);
	}
	
	function onPchartReady_p2()
	{
		onPchartReady(pchart2);
	}
	
	function onPchartReady_p3()
	{
		onPchartReady(pchart3);
	}
	
	function onPchartReady(pchart)
	{
		var legend = document.getElementById(pchart.getOption('legendId'));
		var rows = [];
		var rowsPos = 0;
		
		var dataTable = pchart.getDataTable();
		var colorPallette = pchart.getOption('colors');
		
		var length = dataTable.getNumberOfRows();
		for (var i = 0; i < length; i++)
		{
			var element = document.createElement('tr');
			rows.push(element);
			legend.appendChild(element);
		}
		for (var i = 0; i < length; i++)
		{
			addLegendMarker({
				index: i,
				marker: 'legend-marker-color-circle',
				color: colorPallette[i],
				label: dataTable.getValue(i, 0)
			});
		}

		var markers = legend.getElementsByTagName('td');
		Array.prototype.forEach.call(markers, function(marker)
		{
			while (marker.tagName.toLowerCase() !== 'td')
			{
				marker = marker.parentNode;
			}
			
			marker.chart = pchart.getChart();
			
			marker.addEventListener('click', function (e)
			{
				var columnIndex = parseInt(this.getAttribute('data-columnIndex'));
				this.chart.setSelection([{row: columnIndex}]);

			}, false);

		});
		function addLegendMarker(markerProps)
		{
			var legendMarker = document.getElementById('template-legend-marker').innerHTML;
			for (var handle in markerProps)
			{
				if (markerProps.hasOwnProperty(handle))
				{
					legendMarker = legendMarker.replace('{' + handle + '}', markerProps[handle]);
				}
			}
			rows[rowsPos++].insertAdjacentHTML('beforeEnd', legendMarker);
		}
	}
}