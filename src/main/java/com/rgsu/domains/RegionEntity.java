package com.rgsu.domains;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by vurf on 28.07.2016.
 * Сущность для таблицы региона
 */

@Entity
@Table(name = "region")
public class RegionEntity {

    Long id;
    Long id_district;
    String name;
    Integer rating;
    String assignee;
    Long area;
    Long population;
    String administrative_center;
    Integer number_projects;
    String code;
    String description;
    String linkGerb;
    String linkFlag;


    public RegionEntity() {
    }

    @Id
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "id_district")
    public Long getId_district() {
        return id_district;
    }

    public void setId_district(Long id_district) {
        this.id_district = id_district;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "rating")
    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    @Column(name = "assignee")
    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    @Column(name = "area")
    public Long getArea() {
        return area;
    }

    public void setArea(Long area) {
        this.area = area;
    }

    @Column(name = "population")
    public Long getPopulation() {
        return population;
    }

    public void setPopulation(Long population) {
        this.population = population;
    }

    @Column(name = "administrative_center")
    public String getAdministrative_center() {
        return administrative_center;
    }

    public void setAdministrative_center(String administrative_center) {
        this.administrative_center = administrative_center;
    }

    @Column(name = "number_projects")
    public Integer getNumber_projects() {
        return number_projects;
    }

    public void setNumber_projects(Integer number_projects) {
        this.number_projects = number_projects;
    }

    @Column(name = "code")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(name = "discription")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "link_gerb")
    public String getLinkGerb() {
        return linkGerb;
    }

    public void setLinkGerb(String linkGerb) {
        this.linkGerb = linkGerb;
    }

    @Column(name = "link_flag")
    public String getLinkFlag() {
        return linkFlag;
    }

    public void setLinkFlag(String linkFlag) {
        this.linkFlag = linkFlag;
    }
}
