package com.rgsu.domains;

import javax.persistence.*;

/**
 * Created by vurf on 28.07.2016.
 * Сущность для таблицы ФО
 */

@Entity
@Table(name = "district")
public class DistrictEntity {

    Long id;
    String name;
    Integer area;
    Integer population;
    String assignee;

    public DistrictEntity() {
    }

    @Id
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "area")
    public Integer getArea() {
        return area;
    }

    public void setArea(Integer area) {
        this.area = area;
    }

    @Column(name = "population")
    public Integer getPopulation() {
        return population;
    }

    public void setPopulation(Integer population) {
        this.population = population;
    }

    @Column(name = "assignee")
    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }
}
