package com.rgsu.domains;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by vurf on 28.07.2016.
 * Сущность для таблицы объектов
 */

@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NONE)
@Entity
@Table(name = "project")
public class ObjectEntity {

    Long id;
    Long idRegion;
    Long idCategory;
    String projectName;
    String shortName;
    String subjectRF;
    String levelRelease;
    String sphereRelease;
    String sectorRelease;
    String stateRelease;
    String formRelease;
    String termExecution;
    String relevanceRationale;
    String objectivesImplementation;
    String tasksImplementation;
    String infoPublicPartner;
    String infoPrivatePartner;
    Date dateSigning;
    Date dateFinishing;
    String profitabilityAssessment;
    Long totalFunding;
    String budgetaryFunding;
    String privateFunding;
    String ownPrivateMoney;
    String creditPrivateMoney;
    String termCreditFinishing;
    String riskDescription;
    String efficiency;
    String comparativeAdvantage;
    Long cost;
    Boolean isImplemented;
    Boolean completeAuction;
    String address;
    List<ImageEntity> images;

    public static ObjectEntity makeById(Long id) {
        ObjectEntity entity = new ObjectEntity();
        entity.setId(id);
        entity.setProjectName("Object with id " + id);
        entity.setAddress("moscow h" + id + ", f" + id);
        return entity;
    }

    public ObjectEntity() {
    }


    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "id_region")
    public Long getIdRegion() {
        return idRegion;
    }

    public void setIdRegion(Long idRegion) {
        this.idRegion = idRegion;
    }

    @Column(name = "id_category")
    public Long getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(Long idCategory) {
        this.idCategory = idCategory;
    }

    @JsonSerialize
    @JsonProperty("name")
    @Column(name = "project_name")
    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    @Column(name = "subject_RF")
    public String getSubjectRF() {
        return subjectRF;
    }

    public void setSubjectRF(String subjectRF) {
        this.subjectRF = subjectRF;
    }

    @Column(name = "level_release")
    public String getLevelRelease() {
        return levelRelease;
    }

    public void setLevelRelease(String levelRelease) {
        this.levelRelease = levelRelease;
    }

    @Column(name = "sphere_release")
    public String getSphereRelease() {
        return sphereRelease;
    }

    public void setSphereRelease(String sphereRelease) {
        this.sphereRelease = sphereRelease;
    }

    @Column(name = "sector_release")
    public String getSectorRelease() {
        return sectorRelease;
    }

    public void setSectorRelease(String sectorRelease) {
        this.sectorRelease = sectorRelease;
    }

    @Column(name = "state_release")
    public String getStateRelease() {
        return stateRelease;
    }

    public void setStateRelease(String stateRelease) {
        this.stateRelease = stateRelease;
    }

    @Column(name = "form_release")
    public String getFormRelease() {
        return formRelease;
    }

    public void setFormRelease(String formRelease) {
        this.formRelease = formRelease;
    }

    @Column(name = "date_signing")
    public Date getDateSigning() {
        return dateSigning;
    }

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public void setDateSigning(Date dateSigning) {
        this.dateSigning = dateSigning;
    }

    @Column(name = "term_execution")
    public String getTermExecution() {
        return termExecution;
    }

    public void setTermExecution(String termExecution) {
        this.termExecution = termExecution;
    }

    @Column(name = "relevance_rationale")
    public String getRelevanceRationale() {
        return relevanceRationale;
    }

    public void setRelevanceRationale(String relevanceRationale) {
        this.relevanceRationale = relevanceRationale;
    }

    @Column(name = "objectives_implementation")
    public String getObjectivesImplementation() {
        return objectivesImplementation;
    }

    public void setObjectivesImplementation(String objectivesImplementation) {
        this.objectivesImplementation = objectivesImplementation;
    }

    @Column(name = "tasks_implementation")
    public String getTasksImplementation() {
        return tasksImplementation;
    }

    public void setTasksImplementation(String tasksImplementation) {
        this.tasksImplementation = tasksImplementation;
    }

    @Column(name = "info_public_partner")
    public String getInfoPublicPartner() {
        return infoPublicPartner;
    }

    public void setInfoPublicPartner(String infoPublicPartner) {
        this.infoPublicPartner = infoPublicPartner;
    }

    @Column(name = "info_private_partner")
    public String getInfoPrivatePartner() {
        return infoPrivatePartner;
    }

    public void setInfoPrivatePartner(String infoPrivatePartner) {
        this.infoPrivatePartner = infoPrivatePartner;
    }

    @Column(name = "date_finishing")
    public Date getDateFinishing() {
        return dateFinishing;
    }

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public void setDateFinishing(Date dateFinishing) {
        this.dateFinishing = dateFinishing;
    }

    @Column(name = "profitability_assessment")
    public String getProfitabilityAssessment() {
        return profitabilityAssessment;
    }

    public void setProfitabilityAssessment(String profitabilityAssessment) {
        this.profitabilityAssessment = profitabilityAssessment;
    }

    @Column(name = "total_funding")
    public Long getTotalFunding() {
        return totalFunding;
    }

    public void setTotalFunding(Long totalFunding) {
        this.totalFunding = totalFunding;
    }

    @Column(name = "budgetary_funding")
    public String getBudgetaryFunding() {
        return budgetaryFunding;
    }

    public void setBudgetaryFunding(String budgetaryFunding) {
        this.budgetaryFunding = budgetaryFunding;
    }

    @Column(name = "private_funding")
    public String getPrivateFunding() {
        return privateFunding;
    }

    public void setPrivateFunding(String privateFunding) {
        this.privateFunding = privateFunding;
    }

    @Column(name = "own_private_money")
    public String getOwnPrivateMoney() {
        return ownPrivateMoney;
    }

    public void setOwnPrivateMoney(String ownPrivateMoney) {
        this.ownPrivateMoney = ownPrivateMoney;
    }

    @Column(name = "credit_private_money")
    public String getCreditPrivateMoney() {
        return creditPrivateMoney;
    }

    public void setCreditPrivateMoney(String creditPrivateMoney) {
        this.creditPrivateMoney = creditPrivateMoney;
    }

    @Column(name = "term_credit_finishing")
    public String getTermCreditFinishing() {
        return termCreditFinishing;
    }

    public void setTermCreditFinishing(String termCreditFinishing) {
        this.termCreditFinishing = termCreditFinishing;
    }

    @Column(name = "risk_description")
    public String getRiskDescription() {
        return riskDescription;
    }

    public void setRiskDescription(String riskDescription) {
        this.riskDescription = riskDescription;
    }

    @Column(name = "efficiency")
    public String getEfficiency() {
        return efficiency;
    }

    public void setEfficiency(String efficiency) {
        this.efficiency = efficiency;
    }

    @Column(name = "comparative_advantage")
    public String getComparativeAdvantage() {
        return comparativeAdvantage;
    }

    public void setComparativeAdvantage(String comparativeAdvantage) {
        this.comparativeAdvantage = comparativeAdvantage;
    }

    @Column(name = "cost")
    public Long getCost() {
        return cost;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    @Column(name = "isimplemented")
    public Boolean getImplemented() {
        return isImplemented;
    }

    public void setImplemented(Boolean implemented) {
        isImplemented = implemented;
    }

    @Column(name = "complete_auction")
    public Boolean getCompleteAuction() {
        return completeAuction;
    }

    public void setCompleteAuction(Boolean completeAuction) {
        this.completeAuction = completeAuction;
    }

    @Column(name = "adress")
    public String getAddress() {
        return address;
    }

    public void setAddress(String adress) {
        this.address = adress;
    }

    @Column(name = "short_name")
    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @OneToMany(mappedBy = "project")
    public List<ImageEntity> getImages() {
        return images;
    }

    public void setImages(List<ImageEntity> images) {
        this.images = images;
    }


    private String generalImage;

    @Transient
    public String getGeneralImage() {
        return generalImage;
    }

    public void setGeneralImage(String generalImage) {
        this.generalImage = generalImage;
    }
}
