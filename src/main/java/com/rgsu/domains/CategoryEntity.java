package com.rgsu.domains;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by vurf on 28.07.2016.
 * Сущность для таблицы категории
 */
@Entity
@Table(name = "category_project")
public class CategoryEntity {

    Long id;
    String name;

    public CategoryEntity() {
    }

    @Id
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
