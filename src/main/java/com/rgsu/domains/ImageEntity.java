package com.rgsu.domains;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by vurf on 05.12.2016.
 */

@Entity
@Table(name = "image")
public class ImageEntity {

    Long id;
    String url;
    Boolean isDeleted;
    Date uploaded;
//    Long idProject;
    ObjectEntity project;

    @Id
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_project")
    public ObjectEntity getProject() {
        return project;
    }

    public void setProject(ObjectEntity project) {
        this.project = project;
    }

    @Column(name = "url")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Column(name = "deleted")
    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    @Column(name = "uploaded")
    public Date getUploaded() {
        return uploaded;
    }

    public void setUploaded(Date uploaded) {
        this.uploaded = uploaded;
    }

//    @Column(name = "id_project")
//    public Long getIdProject() {
//        return idProject;
//    }
//
//    public void setIdProject(Long idProject) {
//        this.idProject = idProject;
//    }
}
