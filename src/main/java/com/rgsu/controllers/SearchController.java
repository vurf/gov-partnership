package com.rgsu.controllers;

import com.rgsu.services.ObjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by vurf on 03.12.2016.
 */
@Controller
@RequestMapping("/search")
public class SearchController {

    @Autowired
    private ObjectService objectService;

    @RequestMapping(method = RequestMethod.GET)
    public String getSearchObjects(@RequestParam(value = "text", required = false) String text, Model model) {
        model.addAttribute("objects", this.objectService.findBySearchText(text));
        return "search";
    }
}
