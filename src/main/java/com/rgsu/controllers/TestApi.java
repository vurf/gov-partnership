package com.rgsu.controllers;

import com.rgsu.domains.*;
import com.rgsu.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.*;

/**
 * Created by vurf on 09.11.16.
 * Контроллер для проверки данных в бд.
 */
@RestController
@RequestMapping("/api")
public class TestApi {

    @Autowired
    private AuctionRepository auctionRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private DistrictRepository districtRepository;

    @Autowired
    private ObjectRepository objectRepository;

    @Autowired
    private RegionRepository regionRepository;

    @Autowired
    private NewsRepository newsRepository;

    //Данные из таблицы аукцион
    @RequestMapping(value = "/auction", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<AuctionEntity> getAuctionItems() {
        return this.auctionRepository.findAll();
    }

    //Данные из таблицы категории
    @RequestMapping(value = "/category", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<CategoryEntity> getCategoryItems() {
        return this.categoryRepository.findAll();
    }

    //Данные из таблицы ФО
    @RequestMapping(value = "/district", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<DistrictEntity> getDistrictItems() {
        return this.districtRepository.findAll();
    }

    //Данные из таблицы объекты
    @RequestMapping(value = "/object", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<ObjectEntity> getObjectItems() {
        return this.objectRepository.findAll();
    }

    //Данные из таблицы регионы
    @RequestMapping(value = "/region", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<RegionEntity> getRegionItems() {
        return this.regionRepository.findAll();
    }

    //Данные из таблицы новости
    @RequestMapping(value = "/news", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<NewsEntity> getNewsItems() {
        return this.newsRepository.findAll();
    }
}
