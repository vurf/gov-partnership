package com.rgsu.controllers;

import com.rgsu.domains.DistrictEntity;
import com.rgsu.services.DistrictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by home on 28/07/16.
 * Контроллер для работы с Федеральными Округами
 */

@Controller
@RequestMapping("/districts")
public class DistrictController {

    @Autowired
    private DistrictService districtService;

    //Получение всех ФО
    @RequestMapping(method = RequestMethod.GET)
    public String getAll(Model model) {
        model.addAttribute("districts", this.districtService.getAll());
        return "districts/all";
    }

    //Получение ФО по Id
    @RequestMapping("/{id}")
    public String getItem(@PathVariable Long id, Model model) {
        model.addAttribute("district", this.districtService.getItem(id));
        return "districts/item";
    }

    //Добавление ФО
    @RequestMapping("/new")
    public String getNew(DistrictEntity districtEntity, Model model) {
        return "districts/new";
    }

    //Редактирование ФО
    @RequestMapping("/edit/{id}")
    public String getEdit(@PathVariable Long id, Model model) {
        model.addAttribute("district", this.districtService.getItem(id));
        return "districts/edit";
    }

    //Сохранение отредактированной записи
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST, params = "save")
    public String postEdit(DistrictEntity entity, Model model) {
        this.districtService.save(entity);
        return "redirect:/districts";
    }

    //Удаление записи по id
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST, params = "delete")
    public String postRemove(DistrictEntity entity, Model model) {
        this.districtService.delete(entity);
        return "redirect:/districts";
    }

    //Сохранение новой записи
    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public String postNew(DistrictEntity entity, Model model) {
        this.districtService.save(entity);
        return "redirect:/districts";
    }
}
