package com.rgsu.controllers;

import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.rgsu.services.AwsS3Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * Created by vurf on 04.12.2016.
 */

@Controller
@RequestMapping("/api/aws")
public class UploadController {

    @Autowired
    private AwsS3Component awsService;

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public List<PutObjectResult> upload(@RequestParam("file") MultipartFile[] multipartFiles) {
        return this.awsService.upload(multipartFiles);
    }

    @RequestMapping(value = "/download", method = RequestMethod.GET)
    public byte[] download(@RequestParam String key) throws IOException {
        return this.awsService.download(key);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public List<S3ObjectSummary> list() throws IOException {
        List<S3ObjectSummary> temp = this.awsService.list();
        return temp;
    }

}
