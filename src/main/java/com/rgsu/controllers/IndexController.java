package com.rgsu.controllers;

import com.rgsu.domains.RatingEntity;
import com.rgsu.services.RatingService;
import com.rgsu.services.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by vurf on 27.07.2016.
 * Контроллер, отвечающий за главную страницу
 */

@Controller
public class IndexController {

    @Autowired
    private RegionService regionService;

    @Autowired
    private RatingService ratingService;

    //Главная страница
    @RequestMapping("/")
    public String getIndex(Model model) {
        model.addAttribute("activeClassHome", "active");
        return "index";
    }

    //Пункт рейтинга
    @RequestMapping("/rating")
    public String getRating(Model model) {
        model.addAttribute("activeClassRating", "active");
        List<RatingEntity> ratings = this.ratingService.getTopRegions();
        model.addAttribute("cluster1", ratings.subList(0, 9));
        model.addAttribute("cluster2", ratings.subList(10, 19));
        model.addAttribute("cluster3", ratings.subList(20, 29));
        model.addAttribute("cluster4", ratings.subList(30, 39));
        model.addAttribute("cluster5", ratings.subList(40, 49));
        model.addAttribute("cluster6", ratings.subList(50, 59));
        model.addAttribute("cluster7", ratings.subList(60, 69));
        model.addAttribute("cluster8", ratings.subList(70, 79));
        model.addAttribute("cluster9", ratings.subList(80, ratings.size()));

//        model.addAttribute("items", this.ratingService.getTopRegions());
        return "rating";
    }

    //Пункт карты
    @RequestMapping("/map")
    public String getMap(Model model) {
        model.addAttribute("activeClassMap", "active");
        return "map";
    }

    //Пункт региона
    @RequestMapping("/region")
    public String getRegion(HttpServletRequest request, Model model) {
        model.addAttribute("activeClassRegion", "active");
        model.addAttribute("region", this.regionService.getRegionByIpAddress(request.getRemoteAddr()));
        model.addAttribute("objects", this.regionService.getObjectsByIpAddress(request.getRemoteAddr()));
        return "region";
    }
}
