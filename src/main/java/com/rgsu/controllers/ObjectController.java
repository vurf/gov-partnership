package com.rgsu.controllers;

import com.rgsu.domains.ImageEntity;
import com.rgsu.domains.ObjectEntity;
import com.rgsu.domains.Pager;
import com.rgsu.responses.JsonObject;
import com.rgsu.security.services.SecurityService;
import com.rgsu.services.AwsS3Component;
import com.rgsu.services.ObjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.*;

/**
 * Created by home on 08/08/16.
 * Контроллер для работы с объектами
 */

@Controller
@RequestMapping("/objects")
public class ObjectController {

    private static final int BUTTONS_TO_SHOW = 5;
    private static final int INITIAL_PAGE = 0;
    private static final int INITIAL_PAGE_SIZE = 10;
    private static final int[] PAGE_SIZES = { 5, 10, 20 };

    @Autowired
    private ObjectService objectService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private AwsS3Component storageService;

    public String getAll(Model model) {
        model.addAttribute("activeClassPotential", "active");
        model.addAttribute("objects", this.objectService.getRandomObjects());
        return "objects/all";
    }

    //Генерация списка объектов с постраничным отображением,
    //А также фильтрация для бегущей строки
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView getPaginationObjects(@RequestParam(value = "id", required = false) Integer id,
                                             @RequestParam(value = "pageSize", required = false) Integer pageSize,
                                             @RequestParam(value = "page", required = false) Integer page) {

        ModelAndView modelAndView = new ModelAndView("objects/all");
        int evalPageSize = pageSize == null ? INITIAL_PAGE_SIZE : pageSize;
        int evalPage = (page == null || page < 1) ? INITIAL_PAGE : page - 1;

        Page<ObjectEntity> objects;
        Pager pager;

        if (id == null) id = 0;

        if (id == 1) {
            objects = this.objectService.findBlagoustrPageable(new PageRequest(evalPage, evalPageSize));
            pager = new Pager(objects.getTotalPages(), objects.getNumber(), BUTTONS_TO_SHOW);
        } else if (id == 2) {
            objects = this.objectService.findZdravookhranenie(new PageRequest(evalPage, evalPageSize));
            pager = new Pager(objects.getTotalPages(), objects.getNumber(), BUTTONS_TO_SHOW);
        } else if(id ==3) {
            objects = this.objectService.findKulturi(new PageRequest(evalPage, evalPageSize));
            pager = new Pager(objects.getTotalPages(), objects.getNumber(), BUTTONS_TO_SHOW);
        } else if (id==4) {
            objects = this.objectService.findObrazovanie(new PageRequest(evalPage, evalPageSize));
            pager = new Pager(objects.getTotalPages(), objects.getNumber(), BUTTONS_TO_SHOW);
        } else if (id == 5) {
            objects = this.objectService.findSotsobl(new PageRequest(evalPage, evalPageSize));
            pager = new Pager(objects.getTotalPages(), objects.getNumber(), BUTTONS_TO_SHOW);
        } else if (id == 6) {
            objects = this.objectService.findSport(new PageRequest(evalPage, evalPageSize));
            pager = new Pager(objects.getTotalPages(), objects.getNumber(), BUTTONS_TO_SHOW);
        } else if (id == 7) {
            objects = this.objectService.findTurizm(new PageRequest(evalPage, evalPageSize));
            pager = new Pager(objects.getTotalPages(), objects.getNumber(), BUTTONS_TO_SHOW);
        } else {
            objects = this.objectService.findAllPageable(new PageRequest(evalPage, evalPageSize));
            pager = new Pager(objects.getTotalPages(), objects.getNumber(), BUTTONS_TO_SHOW);
        }

        objects = this.modifyGeneralImageForObjects(objects);

        modelAndView.addObject("activeClassPotential", "active");
        modelAndView.addObject("objects", objects);
        modelAndView.addObject("selectedPageSize", evalPageSize);
        modelAndView.addObject("id", id);
        modelAndView.addObject("pageSizes", PAGE_SIZES);
        modelAndView.addObject("pager", pager);
        return modelAndView;
    }

    //Отображение объекта по его Id
    @RequestMapping("/{id}")
    public String getItem(@PathVariable int id, Model model) throws IOException {
        model.addAttribute("object", this.objectService.getItem(id));
        model.addAttribute("image", this.objectService.getImageUrl(id));
        return "objects/item";
    }

    //Добавление нового объекта
    @RequestMapping("/new")
    public String getNew(Model model) {
        model.addAttribute("objectEntity", new ObjectEntity());
        return "objects/new";
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public String saveNewProject(@ModelAttribute ObjectEntity objectEntity,
                                 @RequestParam("file1") MultipartFile file1,
                                 @RequestParam("file2") MultipartFile file2,
                                 @RequestParam("file3") MultipartFile file3) {
        if (objectEntity == null) {
            return "redirect:/";
        }

        List<MultipartFile> files = new ArrayList<MultipartFile>();
        if (!file1.getOriginalFilename().isEmpty()) {
            files.add(file1);
        }
        if (!file2.getOriginalFilename().isEmpty()) {
            files.add(file2);
        }
        if (!file3.getOriginalFilename().isEmpty()) {
            files.add(file3);
        }

        if (objectEntity.getProjectName() != null && !objectEntity.getProjectName().isEmpty()) {
            long idRegion = this.securityService.getUserProfile().getIdRegion();
            this.objectService.saveNewProject(objectEntity, idRegion, files);
        }

        return "redirect:/my";
    }

    //Редактирование объекта по Id
    @RequestMapping("/edit/{id}")
    public String getEdit(@PathVariable int id, Model model) {
        model.addAttribute("objectEntity", this.objectService.getItem(id));
        model.addAttribute("images", this.objectService.getImagesUrl(id));
        return "objects/edit";
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public String saveEditingObject(@ModelAttribute ObjectEntity objectEntity,
                                    @RequestParam("file1") MultipartFile file1,
                                    @RequestParam("file2") MultipartFile file2,
                                    @RequestParam("file3") MultipartFile file3) {

        if (objectEntity == null) {
            return "redirect:/";
        }

        if (objectEntity.getIdRegion() != this.securityService.getUserProfile().getIdRegion()) {
            return "redirect:/";
        }

        List<MultipartFile> files = new ArrayList<MultipartFile>();
        if (!file1.getOriginalFilename().isEmpty()) {
            files.add(file1);
        }
        if (!file2.getOriginalFilename().isEmpty()) {
            files.add(file2);
        }
        if (!file3.getOriginalFilename().isEmpty()) {
            files.add(file3);
        }

        if (objectEntity.getProjectName() != null && !objectEntity.getProjectName().isEmpty()) {
            long idRegion = this.securityService.getUserProfile().getIdRegion();
            this.objectService.editProject(objectEntity, idRegion, files);
        }

        return "redirect:/my";
    }

    //Генератор рандомных объектов
    @RequestMapping(value = "/generator", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<JsonObject> getRandomObjects() {
        return this.objectService.getRandomObjects();
    }

    private Page<ObjectEntity> modifyGeneralImageForObjects(Page<ObjectEntity> entities) {
        for (ObjectEntity entity : entities) {
            if (!entity.getImages().isEmpty()) {
                entity.setGeneralImage(entity.getImages().get(0).getUrl());
            } else {
                entity.setGeneralImage("/objects/images/" + entity.getId() + ".jpg");
            }
        }
        return entities;
    }
}
