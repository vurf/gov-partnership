package com.rgsu.controllers;

import com.rgsu.services.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by home on 08/08/16.
 * Контроллер региона
 */
@Controller
@RequestMapping("/regions")
public class RegionController {

    @Autowired
    private RegionService regionService;

    //Отображение всех регионов
    @RequestMapping(method = RequestMethod.GET)
    public String getAll(Model model) {
        model.addAttribute("regions", this.regionService.getRegions());
        return "regions/all";
    }

    //Отображение региона по коду
    @RequestMapping("/{code}")
    public String getItem(@PathVariable String code, Model model) {
        model.addAttribute("region", this.regionService.getRegionByCode(code));
        model.addAttribute("objects", this.regionService.getObjectsByRegionCode(code));
        return "regions/item";
    }

    //Отображение региона по Id
    @RequestMapping("/id/{id}")
    public String getItemById(@PathVariable long id, Model model) {
        model.addAttribute("region", this.regionService.getRegionById(id));
        model.addAttribute("objects", this.regionService.getObjectByRegionId(id));
        return "regions/item";
    }

    //Добавление нового региона
    @RequestMapping("/new")
    public String getNew(Model model) {
        return "regions/new";
    }

    //Редактирование региона по Id
    @RequestMapping("/edit/{id}")
    public String getEdit(@PathVariable int id, Model model) {
        return "regions/edit";
    }
}
