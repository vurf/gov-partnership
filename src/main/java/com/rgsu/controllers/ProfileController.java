package com.rgsu.controllers;

import com.rgsu.security.domains.UserEntity;
import com.rgsu.security.services.SecurityService;
import com.rgsu.security.services.UserService;
import com.rgsu.services.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by home on 06/08/16.
 * Контроллер, отвечающий за security-данные
 */
@Controller
@RequestMapping("/my")
public class ProfileController {

    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private RegionService regionService;

    //Показ профиля пользователя (добавление и редактирование объектов)
    @RequestMapping(method = RequestMethod.GET)
    public String my(Model model) {
        UserEntity user = this.securityService.getUserProfile();

        if (user != null) {
            model.addAttribute("name", user.getUsername());
        }
        model.addAttribute("region", this.regionService.getRegionById(user.getIdRegion()));
        model.addAttribute("objects", this.regionService.getObjectByRegionId(user.getIdRegion()));
        model.addAttribute("rating", this.regionService.getRatingByRegionId(user.getIdRegion()));
        return "profile/my";
    }

    //Выход
    @RequestMapping("/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            new SecurityContextLogoutHandler().logout(request,response,authentication);
        }
        return "redirect:/";
    }

    //Авторизация
    @RequestMapping("/login")
    public String login(Model model) {
        return "profile/login";
    }

    //Регистрация (скрытая) отображение
    @RequestMapping("/register")
    public String register(Model model) {
        return  "profile/register";
    }

    //Регистрация - сохранение в бд и перевод на карту
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerPost(@ModelAttribute("userForm") UserEntity userForm, Model model) {
        String password = userForm.getPassword();
        userService.save(userForm);
        securityService.autologin(userForm.getUsername(), password);
        return "redirect:/map";
    }
}
