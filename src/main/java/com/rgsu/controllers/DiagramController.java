package com.rgsu.controllers;

import com.rgsu.services.DiagramService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by vurf on 19.11.16.
 * Контроллер для работы с графиками
 */
@Controller
@RequestMapping("/chart")
public class DiagramController {

    @Autowired
    private DiagramService diagramService;

    //Отображение графика по коду региона
    @RequestMapping("/{code}")
    public String getChartForRegionCode(@PathVariable String code, Model model) {
        model.addAttribute("region", this.diagramService.getDataDiagramByRegionCode(code));
        model.addAttribute("sport", this.diagramService.getSportObjectsCount(code));
        model.addAttribute("zdrav", this.diagramService.getZdravObjectsCount(code));
        model.addAttribute("blag", this.diagramService.getBlagObjectsCount(code));
        model.addAttribute("travel", this.diagramService.getTravelObjectsCount(code));
        model.addAttribute("culture", this.diagramService.getCultureObjectsCount(code));
        model.addAttribute("education", this.diagramService.getEducationObjectsCount(code));
        model.addAttribute("society", this.diagramService.getSocietyObjectsCount(code));
        model.addAttribute("ready", this.diagramService.getReadyObjectsCount(code));
        model.addAttribute("notready", this.diagramService.getNotReadyObjectsCount(code));
        model.addAttribute("mun", this.diagramService.getMunObjectsCount(code));
        model.addAttribute("regional", this.diagramService.getRegObjectsCount(code));
        model.addAttribute("regionfunding", this.diagramService.getRegionFunding(code));
        model.addAttribute("overallfunding",this.diagramService.getOverallFunding());
        model.addAttribute("sportfunding",this.diagramService.getSportObjectsFunding(code));
        model.addAttribute("zdravfunding",this.diagramService.getZdravObjectsFunding(code));
        model.addAttribute("blagfunding",this.diagramService.getBlagObjectsFunding(code));
        model.addAttribute("travelfunding",this.diagramService.getTravelObjectsFunding(code));
        model.addAttribute("culturefunding",this.diagramService.getCultureObjectsFunding(code));
        model.addAttribute("educationfunding",this.diagramService.getEducationObjectsFunding(code));
        model.addAttribute("societyfunding",this.diagramService.getSocietyObjectsFunding(code));
        return "charts/item";
    }
}
