package com.rgsu.config;

import com.rgsu.domains.*;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

/**
 * Created by home on 02/08/16.
 * Конфигурация для соединения с базой данных с сущностями из public схемы
 */

@Configuration
@EnableJpaRepositories(
        basePackages = "com.rgsu.repositories",
        entityManagerFactoryRef = "entityManagerFactory",
        transactionManagerRef = "transactionManager"
)
@EnableTransactionManagement
public class JpaConfig {

    @Value("${hikariCP.driverClassName}")
    private String driverClassName;

    @Value("${hikariCP.jdbcUrl}")
    private String jdbcUrl;

    @Value("${hikariCP.username}")
    private String username;

    @Value("${hikariCP.password}")
    private String password;

    @Value("${hikariCP.jndiConnect}")
    private String jndiConnect;

    @Primary
    @Bean(name = "dataSource")
    public DataSource dataSource() {
        HikariConfig config = new HikariConfig();
        try {
            JndiDataSourceLookup lookup = new JndiDataSourceLookup();
            config.setDataSource(lookup.getDataSource(jndiConnect));
            return new HikariDataSource(config);
        } catch (Exception ex) {
            config.setJdbcUrl(jdbcUrl);
            config.setUsername(username);
            config.setDriverClassName("org.postgresql.Driver");
            config.setPassword(password);
            config.addDataSourceProperty("cachePrepStmts", "true");
            config.addDataSourceProperty("prepStmtCacheSize", "250");
            config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
            return new HikariDataSource(config);
        }

    }

    @Primary
    @Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            final EntityManagerFactoryBuilder builder, @Qualifier("dataSource") DataSource dataSource) {

        return builder.dataSource(dataSource)
                .packages(
                        AuctionEntity.class,
                        CategoryEntity.class,
                        DistrictEntity.class,
                        ObjectEntity.class,
                        RegionEntity.class,
                        NewsEntity.class
                )
                .persistenceUnit("oldUnit")
                .build();
    }

    @Primary
    @Bean(name = "transactionManager")
    public PlatformTransactionManager transactionManager(@Qualifier("entityManagerFactory")EntityManagerFactory entityManagerFactory) {
        return  new JpaTransactionManager(entityManagerFactory);
    }
}
