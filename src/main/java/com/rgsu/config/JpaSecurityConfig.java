package com.rgsu.config;

import com.rgsu.security.domains.RoleEntity;
import com.rgsu.security.domains.UserEntity;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

/**
 * Created by home on 05/08/16.
 * Конфигурация для соединения с базой данных для сущностей из security схемы
 */

@Configuration
@EnableJpaRepositories(
        basePackages = "com.rgsu.security",
        entityManagerFactoryRef = "secEntityManagerFactory",
        transactionManagerRef = "secTransactionManager"
)
public class JpaSecurityConfig  {

    @Value("${hikariCP.driverClassName}")
    private String driverClassName;

    @Value("${hikariCP.sec.jdbcUrl}")
    private String jdbcUrl;

    @Value("${hikariCP.sec.username}")
    private String username;

    @Value("${hikariCP.sec.password}")
    private String password;

    @Value("${hikariCP.sec.jndiConnect}")
    private String jndiConnect;

    @Bean(name = "secDataSource")
    public DataSource secDataSource() {
        HikariConfig config = new HikariConfig();
        try {
            JndiDataSourceLookup lookup = new JndiDataSourceLookup();
            config.setDataSource(lookup.getDataSource(jndiConnect));
            return new HikariDataSource(config);
        } catch (Exception ex) {
            config.setJdbcUrl(jdbcUrl);
            config.setUsername(username);
            config.setPassword(password);
            config.setDriverClassName("org.postgresql.Driver");
            config.addDataSourceProperty("cachePrepStmts", "true");
            config.addDataSourceProperty("prepStmtCacheSize", "250");
            config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
            return new HikariDataSource(config);
        }

    }

    @Bean(name = "secEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean secEntityManagerFactory(
            final EntityManagerFactoryBuilder builder, @Qualifier("secDataSource") DataSource dataSource) {

        return builder.dataSource(dataSource)
                .packages(
                        UserEntity.class,
                        RoleEntity.class
                )
                .persistenceUnit("secUnit")
                .build();
    }

    @Bean(name = "secTransactionManager")
    public PlatformTransactionManager secTransactionManager(@Qualifier("secEntityManagerFactory")EntityManagerFactory entityManagerFactory) {
        return  new JpaTransactionManager(entityManagerFactory);
    }

}
