package com.rgsu.config;

import com.rgsu.security.services.UserDetailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;

import javax.sql.DataSource;

/**
 * Created by home on 05/08/16.
 * Ограничение в доступе к страницам для гостей и авторизованных пользователей
 */
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/my",
                        "/regions/edit/*", "/regions/new",
                        "/objects/edit/*", "/objects/new"
                ).authenticated()
                .anyRequest().permitAll()
                .and()
                .formLogin().loginPage("/my/login")
                .and()
                .exceptionHandling().accessDeniedPage("/error")
                .and().
                logout().permitAll();
    }

    @Autowired
    private UserDetailServiceImpl userDetailService;

    @Autowired
    private DataSource dataSource;

    @Bean
    PasswordEncoder passwordEncoder() {
        return new StandardPasswordEncoder();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .dataSource(dataSource)
                .passwordEncoder(passwordEncoder())
                .usersByUsernameQuery("select username, password, enabled from security.users where username = ?")
                .authoritiesByUsernameQuery("select username, role from security.user_roles where username = ?");
    }

}
