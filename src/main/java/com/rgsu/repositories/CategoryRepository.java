package com.rgsu.repositories;

import com.rgsu.domains.CategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by vurf on 09.11.16.
 * Репозиторий для работы с бд
 */
public interface CategoryRepository extends JpaRepository<CategoryEntity, Long> {
}
