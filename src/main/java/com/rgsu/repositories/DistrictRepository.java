package com.rgsu.repositories;

import com.rgsu.domains.DistrictEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by home on 02/08/16.
 * Репозиторий для работы с бд
 */

public interface DistrictRepository extends JpaRepository<DistrictEntity, Long> {

}
