package com.rgsu.repositories;

import com.rgsu.domains.ImageEntity;
import com.rgsu.domains.ObjectEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


/**
 * Created by vurf on 05.12.2016.
 */
public interface ImageRepository extends JpaRepository<ImageEntity, Long> {

    @Query("from ImageEntity where project = :project")
    List<ImageEntity> findImageEntitiesByIdProject(@Param("project") ObjectEntity project);

    @Query("SELECT max(t.id) FROM ImageEntity t")
    Long getMaxId();
}
