package com.rgsu.repositories;

import com.rgsu.domains.ObjectEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.method.P;

import java.util.List;

/**
 * Created by vurf on 09.11.16.
 * Репозиторий для работы с бд
 */
public interface ObjectRepository extends JpaRepository<ObjectEntity, Long> {

    @Query("from ObjectEntity where id = :id")
    ObjectEntity findById(@Param("id") long id);

    @Query("from ObjectEntity where idRegion = :regionId")
    List<ObjectEntity> findByRegionId(@Param("regionId") long regionId);

    @Query("from ObjectEntity where sectorRelease = :nn")
    List<ObjectEntity> findBySectorRelease(@Param("nn") String nn);

    @Query("from ObjectEntity where idRegion = :regionId and sectorRelease = :sector")
    List<ObjectEntity> findByRegionIdAndSectorRelease(@Param("regionId") long regionId, @Param("sector") String sectorRelease);

    @Query("from ObjectEntity where idRegion = :id and sectorRelease = :sr")
    List<ObjectEntity> findByIdAndSector(@Param("id") Long id,@Param("sr") String sr);

    @Query("from ObjectEntity where idRegion = :id and stateRelease = :sr")
    List<ObjectEntity> findByIdAndState(@Param("id") Long id,@Param("sr") String sr);

    @Query("from ObjectEntity where idRegion = :id and stateRelease != :sr")
    List<ObjectEntity> findByIdAndNotState(@Param("id") Long id,@Param("sr") String sr);

    @Query("from ObjectEntity where idRegion = :id and levelRelease != :sr")
    List<ObjectEntity> findByIdAndLevel(@Param("id") Long id,@Param("sr") String sr);
}
