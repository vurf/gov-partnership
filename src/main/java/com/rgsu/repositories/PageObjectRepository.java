package com.rgsu.repositories;

import com.rgsu.domains.ObjectEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

/**
 * Created by vurf on 22.11.2016.
 * Репозиторий для работы с бд
 */

public interface PageObjectRepository extends PagingAndSortingRepository<ObjectEntity, Long> {

    Page<ObjectEntity> findBySectorRelease(String sectorRelease, Pageable pageable);
}
