package com.rgsu.repositories;

import com.rgsu.domains.RegionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by vurf on 09.11.16.
 * Репозиторий для работы с бд
 */

public interface RegionRepository extends JpaRepository<RegionEntity, Long> {

    @Query("from RegionEntity where code = :code")
    RegionEntity getRegionByCode(@Param("code") String code);

    @Query("from RegionEntity where name = :name")
    RegionEntity getRegionByName(@Param("name") String name);
}
