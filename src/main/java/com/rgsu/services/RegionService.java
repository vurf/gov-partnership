package com.rgsu.services;

/**
 * Created by home on 10/08/16.
 * Сервис для взаимодействия с контроллером "Регион"
 */

import com.rgsu.domains.IpLocation;
import com.rgsu.domains.ObjectEntity;
import com.rgsu.domains.RegionEntity;
import com.rgsu.repositories.ObjectRepository;
import com.rgsu.repositories.RegionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

@Service
public class RegionService {

    @Autowired
    private RegionRepository regionRepository;

    @Autowired
    private ObjectRepository objectRepository;

    public RegionEntity getRegionByIpAddress(String ip) {
        IpLocation location = this.getLocationByIpAddress(ip);
        return this.regionRepository.getRegionByName(location.getRegion());
    }

    public List<ObjectEntity> getObjectsByIpAddress(String ip) {
        IpLocation location = this.getLocationByIpAddress(ip);
        List<ObjectEntity> entitis = this.objectRepository.findByRegionId(this.regionRepository.getRegionByName(location.getRegion()).getId());
        return this.modifyGeneralImageForObjects(entitis);
    }

    public List<RegionEntity> getRegions() {
        return  this.regionRepository.findAll();
    }

    public RegionEntity getRegionByCode(String code) {
        return  this.regionRepository.getRegionByCode(code);
    }

    public RegionEntity getRegionById(long id) {
        return  this.regionRepository.getOne(id);
    }

    public List<ObjectEntity> getObjectsByRegionCode(String code) {
        List<ObjectEntity> entities = this.objectRepository.findByRegionId(this.regionRepository.getRegionByCode(code).getId());
        return this.modifyGeneralImageForObjects(entities);
    }

    public List<ObjectEntity> getObjectByRegionId(long id) {
        List<ObjectEntity> entities = this.objectRepository.findByRegionId(id);
        return this.modifyGeneralImageForObjects(entities);
    }

    public double getRatingByRegionId(long idRegion) {
        RegionEntity regionEntity = this.regionRepository.findOne(idRegion);
        long sum = 0;
        List<ObjectEntity> objectsCurrentRegion = this.objectRepository.findByRegionId(regionEntity.getId());
        for (ObjectEntity object : objectsCurrentRegion) {
            sum += object.getTotalFunding();
        }

        double p = (double) regionEntity.getPopulation() / (double) regionEntity.getArea();
        long c = regionEntity.getNumber_projects();
        double index = (double) sum / (p * (double) c);
        return index / 1000;
    }

    public static long GetIdByCode(String code) {
        long id = 1;
        switch (code) {
            case "al":
                id = 1;
                break;
            case "am":
                id = 2;
                break;
            case "ar":
                id = 3;
                break;
            case "as":
                id = 4;
                break;
            case "bl":
                id = 5;
                break;
            case "bn":
                id = 6;
                break;
            case "vm":
                id =7;
                break;
            case "vl":
                id = 8;
                break;
            case "vo":
                id = 9;
                break;
            case "vn":
                id = 10;
                break;
            case "eu":
                id = 11;
                break;
            case "zb":
                id = 12;
                break;
            case "iv":
                id = 13;
                break;
            case "ir":
                id = 14;
                break;
            case "kb":
                id = 15;
                break;
            case "kn":
                id = 16;
                break;
            case "kj":
                id = 17;
                break;
            case "ka":
                id = 18;
                break;
            case "kc":
                id = 19;
                break;
            case "km":
                id = 20;
                break;
            case "ki":
                id = 21;
                break;
            case "kt":
                id = 22;
                break;
            case "ks":
                id = 23;
                break;
            case "kr":
                id = 24;
                break;
            case "ku":
                id = 25;
                break;
            case "ky":
                id = 26;
                break;
            case "le":
                id = 27;
                break;
            case "lp":
                id = 28;
                break;
            case "ma":
                id = 29;
                break;
            case "mccity":
                id = 30; //Москва
                break;
            case "mc":
                id = 31; //Московская область
                break;
            case "mu":
                id = 32;
                break;
            case "ne":
                id = 33;
                break;
            case "nn":
                id = 34;
                break;
            case "no":
                id = 35;
                break;
            case "nv":
                id = 36;
                break;
            case "om":
                id = 37;
                break;
            case "ob":
                id = 38;
                break;
            case "or":
                id = 39;
                break;
            case "pz":
                id = 40;
                break;
            case "pe":
                id = 41;
                break;
            case "pr":
                id = 42;
                break;
            case "ps":
                id = 43;
                break;
            case "ad":
                id = 44;
                break;
            case "lt":
                id = 45;
                break;
            case "bs":
                id = 46;
                break;
            case "br":
                id = 47;
                break;
            case "da":
                id = 48;
                break;
            case "in":
                id = 49;
                break;
            case "kk":
                id = 50;
                break;
            case "kl":
                id = 51;
                break;
            case "ko":
                id = 52;
                break;
            case "cr":
                id = 53;
                break;
            case "ml":
                id = 54;
                break;
            case "mr":
                id = 55;
                break;
            case "sa":
                id = 56;
                break;
            case "so":
                id = 57;
                break;
            case "ta":
                id = 58;
                break;
            case "tv":
                id = 59;
                break;
            case "hk":
                id = 60;
                break;
            case "ro":
                id = 61;
                break;
            case "rz":
                id = 62;
                break;
            case "ss":
                id = 63;
                break;
            case "lecity":
                id = 64; //Санкт-Петербург
                break;
            case "sr":
                id = 65;
                break;
            case "sh":
                id = 66;
                break;
            case "sv":
                id = 67;
                break;
            case "crcity":
                id = 68; //Севастополь
                break;
            case "sm":
                id = 69;
                break;
            case "st":
                id = 70;
                break;
            case "tb":
                id = 71;
                break;
            case "tr":
                id = 72;
                break;
            case "tm":
                id = 73;
                break;
            case "tl":
                id = 74;
                break;
            case "tu":
                id = 75;
                break;
            case "ud":
                id = 76;
                break;
            case "ul":
                id = 77;
                break;
            case "ha":
                id = 78;
                break;
            case "ht":
                id = 79;
                break;
            case "cl":
                id = 80;
                break;
            case "cc":
                id = 81;
                break;
            case "cu":
                id = 82;
                break;
            case "ch":
                id = 83;
                break;
            case "ya":
                id = 84;
                break;
            case "yr":
                id = 85;
                break;
        }
        return id;
    }

    private IpLocation getLocationByIpAddress(String ip) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("windows-1251")));
        String url = "http://ipgeobase.ru:7020/geo?ip="+ip;
        String answerXml = restTemplate.getForObject(url, String.class);

        IpLocation location = new IpLocation();

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            InputSource is = new InputSource(new StringReader(answerXml));
            Document doc = builder.parse(is);

            NodeList nodeList = doc.getElementsByTagName("ip");
            Node node = nodeList.item(0);
            Element element = (Element) node;

            location.setCity(element.getElementsByTagName("city").item(0).getTextContent());
            location.setCountry(element.getElementsByTagName("country").item(0).getTextContent());
            location.setRegion(element.getElementsByTagName("region").item(0).getTextContent());
            location.setDistrict(element.getElementsByTagName("district").item(0).getTextContent());
            location.setLatitude(element.getElementsByTagName("lat").item(0).getTextContent());
            location.setLongitude(element.getElementsByTagName("lng").item(0).getTextContent());
            location.setIp(ip);

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return location;
    }

    private List<ObjectEntity> modifyGeneralImageForObjects(List<ObjectEntity> entities) {
        for (ObjectEntity entity : entities) {
            if (!entity.getImages().isEmpty()) {
                entity.setGeneralImage(entity.getImages().get(0).getUrl());
            } else {
                entity.setGeneralImage("/objects/images/" + entity.getId() + ".jpg");
            }
        }
        return entities;
    }
}
