package com.rgsu.services;

import com.amazonaws.services.s3.model.PutObjectResult;
import com.rgsu.domains.ImageEntity;
import com.rgsu.domains.ObjectEntity;
import com.rgsu.repositories.ImageRepository;
import com.rgsu.repositories.ObjectRepository;
import com.rgsu.repositories.PageObjectRepository;
import com.rgsu.repositories.RegionRepository;
import com.rgsu.responses.FileWrapper;
import com.rgsu.responses.JsonObject;
import com.rgsu.security.UserRepository;
import com.rgsu.security.services.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by vurf on 18.10.16.
 * Сервис для взаимодействия с объектами (проектами)
 */
@Service
public class ObjectService {

    @Autowired
    private ObjectRepository objectRepository;

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private RegionRepository regionRepository;

    @Autowired
    private PageObjectRepository pageObjectRepository;

    @Autowired
    private AwsS3Component storageComponent;

    public ObjectEntity getItem(long id) {
        ObjectEntity entity = this.objectRepository.getOne(id);
        return entity;
    }

    public String getImageUrl(long idObject) {
        ObjectEntity entity = this.objectRepository.getOne(idObject);
        if (entity.getImages() != null && !entity.getImages().isEmpty() && entity.getImages().get(0) != null) {
            return entity.getImages().get(0).getUrl();
        }

        return "/objects/images/" + entity.getId() + ".jpg";
    }

    public List<String> getImagesUrl(long idObject) {
        ObjectEntity entity = this.objectRepository.getOne(idObject);
        List<String> urls = new ArrayList<>();
        if (!entity.getImages().isEmpty()) {
            for (ImageEntity image : entity.getImages()) {
                urls.add(image.getUrl());
            }
        }
        return urls;
    }

    private int maxLength = 100;

    public List<JsonObject> getRandomObjects() {
        this.maxLength = this.objectRepository.findAll().size();
        List<JsonObject> responseItems = new LinkedList<JsonObject>();

        for (int i = 0; i < 10; i++) {
            Random random = new Random();
            int r = random.nextInt(this.maxLength);
            ObjectEntity object = this.objectRepository.findById(r);
            if (object != null) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.setId(object.getId());
                jsonObject.setLink("objects/" + object.getId());

                if (!object.getImages().isEmpty()) {
                    jsonObject.setImageUrl(object.getImages().get(0).getUrl());
                } else {
                    jsonObject.setImageUrl("objects/images/" + object.getId() + ".jpg");
                }

                if (object.getShortName() != null && !object.getShortName().isEmpty()) {
                    jsonObject.setName(object.getShortName());
                } else {
                    jsonObject.setName(object.getProjectName());
                }
                responseItems.add(jsonObject);
            }
        }

        return responseItems;
    }

    public Page<ObjectEntity> findAllPageable(Pageable pageable) {
        return this.pageObjectRepository.findAll(pageable);
    }

    public Page<ObjectEntity> findBlagoustrPageable(Pageable pageable) {
        return this.pageObjectRepository.findBySectorRelease("Благоустройство территорий", pageable);
    }

    public Page<ObjectEntity> findZdravookhranenie(Pageable pageable) {
        return this.pageObjectRepository.findBySectorRelease("Здравоохранение", pageable);
    }

    public Page<ObjectEntity> findKulturi(Pageable pageable) {
        return this.pageObjectRepository.findBySectorRelease("Культура", pageable);
    }

    public Page<ObjectEntity> findObrazovanie(Pageable pageable) {
        return this.pageObjectRepository.findBySectorRelease("Образование", pageable);
    }

    public Page<ObjectEntity> findSotsobl(Pageable pageable) {
        return this.pageObjectRepository.findBySectorRelease("Социальное обслуживание населения", pageable);
    }

    public Page<ObjectEntity> findSport(Pageable pageable) {
        return this.pageObjectRepository.findBySectorRelease("Спорт", pageable);
    }

    public Page<ObjectEntity> findTurizm(Pageable pageable) {
        return this.pageObjectRepository.findBySectorRelease("Туризм", pageable);
    }

    public void editProject(ObjectEntity objectEntity, long idRegion, List<MultipartFile> files) {
        objectEntity.setIdRegion(idRegion);
        objectEntity.setSubjectRF(this.regionRepository.findOne(idRegion).getName());
        this.objectRepository.saveAndFlush(objectEntity);

        List<ImageEntity> removeImages =  this.imageRepository.findImageEntitiesByIdProject(objectEntity);
        if (!removeImages.isEmpty()) {
            for (ImageEntity removeImage : removeImages) {
                this.imageRepository.delete(removeImage.getId());
            }
        }


        List<FileWrapper> fileWrappers = new ArrayList<>();
        for (MultipartFile file: files) {
            String filename = UUID.randomUUID().toString() + "." + file.getOriginalFilename().split("\\.")[1];
            fileWrappers.add(new FileWrapper(file, filename));
        }

        Long lastId = this.imageRepository.getMaxId();
        if (lastId == null) {
            lastId = 0l;
        }

        List<ImageEntity> images = new ArrayList<>();
        List<PutObjectResult> objects = this.storageComponent.upload(fileWrappers);
        for (int i = 0; i < fileWrappers.size(); i++) {
            ImageEntity image = new ImageEntity();
            image.setProject(objectEntity);
            image.setUploaded(new Date());
            image.setId(lastId);
            image.setUrl("https://rgsu-partners-project-images.s3.amazonaws.com/" + fileWrappers.get(i).getName());
            image.setDeleted(false);
            images.add(image);
            lastId++;
        }

        this.imageRepository.save(images);
    }

    public void saveNewProject(ObjectEntity objectEntity, long idRegion, List<MultipartFile> files) {
        objectEntity.setIdRegion(idRegion);
        objectEntity.setSubjectRF(this.regionRepository.findOne(idRegion).getName());
        this.objectRepository.saveAndFlush(objectEntity);

        List<FileWrapper> fileWrappers = new ArrayList<>();
        for (MultipartFile file: files) {
            String filename = UUID.randomUUID().toString() + "." + file.getOriginalFilename().split("\\.")[1];
            fileWrappers.add(new FileWrapper(file, filename));
        }

        Long lastId = this.imageRepository.getMaxId();

        if (lastId == null) {
            lastId = 0l;
        }

        List<ImageEntity> images = new ArrayList<>();
        List<PutObjectResult> objects = this.storageComponent.upload(fileWrappers);
        for (int i = 0; i < fileWrappers.size(); i++) {
            ImageEntity image = new ImageEntity();
            image.setProject(objectEntity);
            image.setId(lastId);
            image.setUploaded(new Date());
            image.setUrl("https://rgsu-partners-project-images.s3.amazonaws.com/" + fileWrappers.get(i).getName());
            image.setDeleted(false);
            images.add(image);
            lastId++;
        }

        this.imageRepository.save(images);
    }

    public void saveProject(ObjectEntity objectEntity) {
        this.objectRepository.save(objectEntity);
    }

    public List<ObjectEntity> findBySearchText(String text) {
        List<ObjectEntity> objectEntities = new ArrayList<ObjectEntity>();
        objectEntities = this.objectRepository.findAll().stream()
                .filter(x->x.getProjectName().toLowerCase().contains(text.toLowerCase()) || x.getShortName().toLowerCase().contains(text.toLowerCase()))
                .collect(Collectors.toList());

        return this.modifyGeneralImageForObjects(objectEntities);
    }

    private List<ObjectEntity> modifyGeneralImageForObjects(List<ObjectEntity> entities) {
        for (ObjectEntity entity : entities) {
            if (!entity.getImages().isEmpty()) {
                entity.setGeneralImage(entity.getImages().get(0).getUrl());
            } else {
                entity.setGeneralImage("/objects/images/" + entity.getId() + ".jpg");
            }
        }
        return entities;
    }

}
