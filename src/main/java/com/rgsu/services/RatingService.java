package com.rgsu.services;

import com.rgsu.domains.ObjectEntity;
import com.rgsu.domains.RatingEntity;
import com.rgsu.domains.RegionEntity;
import com.rgsu.repositories.ObjectRepository;
import com.rgsu.repositories.RegionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vurf on 19.11.2016.
 * Сервис для формирования рейтинга
 */
@Service
public class RatingService {

    @Autowired
    private RegionRepository regionRepository;

    @Autowired
    private ObjectRepository objectRepository;

    public List<RatingEntity> getTopRegions() {
        List<RegionEntity> regions = new ArrayList<RegionEntity>();
        regions = this.regionRepository.findAll();

        List<RatingEntity> response = new ArrayList<RatingEntity>();
        for (int i = 0; i < regions.size(); i++) {
            RegionEntity region = regions.get(i);
            RatingEntity rating = new RatingEntity();
            rating.setId(region.getId());
            rating.setName(region.getName());
            rating.setCode(region.getCode());

            long sum = 0;
            List<ObjectEntity> objectsCurrentRegion = this.objectRepository.findByRegionId(region.getId());
            for (ObjectEntity object : objectsCurrentRegion) {
                sum += object.getTotalFunding();
            }

            rating.setProjectsCount(objectsCurrentRegion.size());

            if (region.getNumber_projects() != 0) {
                double p = (double) region.getPopulation() / (double) region.getArea();
                long c = region.getNumber_projects();
                double index = (double) sum / (p * (double) c);
                rating.setIndex(index / 1000);
            } else {
                rating.setIndex(0d);
            }
            response.add(rating);
        }

        response.sort((o1, o2) -> o2.getIndex().compareTo(o1.getIndex()));
        return response;
    }
}
