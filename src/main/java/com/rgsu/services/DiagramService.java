package com.rgsu.services;

import com.rgsu.domains.ObjectEntity;
import com.rgsu.domains.RegionEntity;
import com.rgsu.repositories.ObjectRepository;
import com.rgsu.repositories.RegionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by vurf on 19.11.16.
 * Сервис получения данных для диаграмм
 */
@Service
public class DiagramService {

    @Autowired
    private RegionRepository regionRepository;

    @Autowired
    private ObjectRepository objectRepository;

    public RegionEntity getDataDiagramByRegionCode(String code) {
        RegionEntity currentRegion = this.regionRepository.getRegionByCode(code);
        return  currentRegion;
    }


    public int getSportObjectsCount(String code) {
        RegionEntity currentRegion = this.regionRepository.getRegionByCode(code);
        List<ObjectEntity> ent = this.objectRepository.findByIdAndSector(currentRegion.getId(),"Спорт");
        int c = ent.size();
        return c;
    }

    public int getZdravObjectsCount(String code) {
        RegionEntity currentRegion = this.regionRepository.getRegionByCode(code);
        List<ObjectEntity> ent = this.objectRepository.findByIdAndSector(currentRegion.getId(),"Здравоохранение");
        int c = ent.size();
        return c;
    }

    public int getBlagObjectsCount(String code) {
        RegionEntity currentRegion = this.regionRepository.getRegionByCode(code);
        List<ObjectEntity> ent = this.objectRepository.findByIdAndSector(currentRegion.getId(),"Благоустройство территорий");
        int c = ent.size();
        return c;
    }

    public int getTravelObjectsCount(String code) {
        RegionEntity currentRegion = this.regionRepository.getRegionByCode(code);
        List<ObjectEntity> ent = this.objectRepository.findByIdAndSector(currentRegion.getId(),"Туризм");
        int c = ent.size();
        return c;
    }

    public int getCultureObjectsCount(String code) {
        RegionEntity currentRegion = this.regionRepository.getRegionByCode(code);
        List<ObjectEntity> ent = this.objectRepository.findByIdAndSector(currentRegion.getId(),"Культура");
        int c = ent.size();
        return c;
    }

    public int getEducationObjectsCount(String code) {
        RegionEntity currentRegion = this.regionRepository.getRegionByCode(code);
        List<ObjectEntity> ent = this.objectRepository.findByIdAndSector(currentRegion.getId(),"Образование");
        int c = ent.size();
        return c;
    }

    public int getSocietyObjectsCount(String code) {
        RegionEntity currentRegion = this.regionRepository.getRegionByCode(code);
        List<ObjectEntity> ent = this.objectRepository.findByIdAndSector(currentRegion.getId(),"Социальное обслуживание населения");
        int c = ent.size();
        return c;
    }

    public int getReadyObjectsCount(String code) {
        RegionEntity currentRegion = this.regionRepository.getRegionByCode(code);
        List<ObjectEntity> ent = this.objectRepository.findByIdAndState(currentRegion.getId(),"Эксплуатационный");
        int c = ent.size();
        return c;
    }

    public int getNotReadyObjectsCount(String code) {
        RegionEntity currentRegion = this.regionRepository.getRegionByCode(code);
        List<ObjectEntity> ent = this.objectRepository.findByIdAndNotState(currentRegion.getId(),"Эксплуатационный");
        int c = ent.size();
        return c;
    }

    public int getMunObjectsCount(String code) {
        RegionEntity currentRegion = this.regionRepository.getRegionByCode(code);
        List<ObjectEntity> ent = this.objectRepository.findByIdAndLevel(currentRegion.getId(),"Муниципальный");
        int c = ent.size();
        return c;
    }

    public int getRegObjectsCount(String code) {
        RegionEntity currentRegion = this.regionRepository.getRegionByCode(code);
        List<ObjectEntity> ent = this.objectRepository.findByIdAndLevel(currentRegion.getId(),"Региональный");
        int c = ent.size();
        return c;
    }

    public Long getRegionFunding(String code){
        RegionEntity currentRegion = this.regionRepository.getRegionByCode(code);
        List<ObjectEntity> ent = this.objectRepository.findByRegionId(currentRegion.getId());
        Long RegionFunding=0L;
        for(ObjectEntity item:ent){
            RegionFunding += item.getTotalFunding();
        }
        return RegionFunding;
    }

    public Long getOverallFunding(){
        List<ObjectEntity> ent = this.objectRepository.findAll();
        Long OverallFunding = 0L;
        for(ObjectEntity item:ent){
            OverallFunding += item.getTotalFunding();
        }
        return OverallFunding;
    }

    public Long getSportObjectsFunding(String code) {
        RegionEntity currentRegion = this.regionRepository.getRegionByCode(code);
        List<ObjectEntity> ent = this.objectRepository.findByIdAndSector(currentRegion.getId(),"Спорт");
        Long SportFunding = 0L;
        for(ObjectEntity item:ent){
            SportFunding += item.getTotalFunding();
        }
        return SportFunding;
    }

    public Long getZdravObjectsFunding(String code) {
        RegionEntity currentRegion = this.regionRepository.getRegionByCode(code);
        List<ObjectEntity> ent = this.objectRepository.findByIdAndSector(currentRegion.getId(),"Здравоохранение");
        Long ZdravFunding = 0L;
        for(ObjectEntity item:ent){
            ZdravFunding += item.getTotalFunding();
        }
        return ZdravFunding;
    }

    public Long getBlagObjectsFunding(String code) {
        RegionEntity currentRegion = this.regionRepository.getRegionByCode(code);
        List<ObjectEntity> ent = this.objectRepository.findByIdAndSector(currentRegion.getId(),"Благоустройство территорий");
        Long BlagFunding = 0L;
        for(ObjectEntity item:ent){
            BlagFunding += item.getTotalFunding();
        }
        return BlagFunding;
    }

    public Long getTravelObjectsFunding(String code) {
        RegionEntity currentRegion = this.regionRepository.getRegionByCode(code);
        List<ObjectEntity> ent = this.objectRepository.findByIdAndSector(currentRegion.getId(),"Туризм");
        Long TravelFunding = 0L;
        for(ObjectEntity item:ent){
            TravelFunding += item.getTotalFunding();
        }
        return TravelFunding;
    }

    public Long getCultureObjectsFunding(String code) {
        RegionEntity currentRegion = this.regionRepository.getRegionByCode(code);
        List<ObjectEntity> ent = this.objectRepository.findByIdAndSector(currentRegion.getId(),"Культура");
        Long CultureFunding = 0L;
        for(ObjectEntity item:ent){
            CultureFunding += item.getTotalFunding();
        }
        return CultureFunding;
    }

    public Long getEducationObjectsFunding(String code) {
        RegionEntity currentRegion = this.regionRepository.getRegionByCode(code);
        List<ObjectEntity> ent = this.objectRepository.findByIdAndSector(currentRegion.getId(),"Образование");
        Long EducationFunding = 0L;
        for(ObjectEntity item:ent){
            EducationFunding += item.getTotalFunding();
        }
        return EducationFunding;
    }

    public Long getSocietyObjectsFunding(String code) {
        RegionEntity currentRegion = this.regionRepository.getRegionByCode(code);
        List<ObjectEntity> ent = this.objectRepository.findByIdAndSector(currentRegion.getId(),"Социальное обслуживание населения");
        Long SocietyFunding = 0L;
        for(ObjectEntity item:ent){
            SocietyFunding += item.getTotalFunding();
        }
        return SocietyFunding;
    }

}
