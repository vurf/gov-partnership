package com.rgsu.services;

import com.rgsu.domains.DistrictEntity;
import com.rgsu.repositories.DistrictRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/**
 * Created by vurf on 28.07.2016.
 * Сервис для взаимодействия с ФО
 */

@Service
public class DistrictService {

    @Autowired
    private DistrictRepository districtRepository;

    public List<DistrictEntity> getAll() {
        List<DistrictEntity> districtEntities = this.districtRepository.findAll();
        Collections.sort(districtEntities, (DistrictEntity d1, DistrictEntity d2)-> d1.getId().compareTo(d2.getId()));
        return districtEntities;
    }

    public DistrictEntity getItem(Long id) {
        return this.districtRepository.getOne(id);
    }

    public void save(DistrictEntity entity) {
        this.districtRepository.save(entity);
    }

    public void delete(DistrictEntity entity) {
        this.districtRepository.delete(entity);
    }

}
