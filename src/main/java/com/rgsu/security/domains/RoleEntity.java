package com.rgsu.security.domains;

import javax.persistence.*;

/**
 * Created by home on 05/08/16.
 * Сущность для таблицы прав пользователей
 */
@Entity
@Table(name = "user_roles", schema = "security", catalog = "partners")
public class RoleEntity {
    Long id;
    String username;
    String role;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "role")
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
