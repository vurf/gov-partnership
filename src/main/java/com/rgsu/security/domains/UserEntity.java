package com.rgsu.security.domains;

import com.rgsu.security.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by home on 05/08/16.
 * Сущность для таблицы пользователей
 */
@Entity
@Table(name = "users", schema = "security", catalog = "partners")
public class UserEntity {
    Long id;
    String username;
    String password;
    String passwordConfirm;
    Long idRegion;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Transient
    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    @Autowired
    RoleRepository roleRepository;

    @Transient
    public Set<RoleEntity> getRole() {
        return roleRepository.findByUsername(username);
    }

    @Column(name = "id_region")
    public Long getIdRegion() {
        return idRegion;
    }

    public void setIdRegion(Long idRegion) {
        this.idRegion = idRegion;
    }
}
