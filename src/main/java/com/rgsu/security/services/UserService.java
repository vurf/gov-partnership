package com.rgsu.security.services;

import com.rgsu.security.domains.RoleEntity;
import com.rgsu.security.domains.UserEntity;
import com.rgsu.security.RoleRepository;
import com.rgsu.security.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * Created by home on 05/08/16.
 * Сервис для взаимодействия с пользователем
 */

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public void save(UserEntity userEntity) {
        userEntity.setPassword(passwordEncoder.encode(userEntity.getPassword()));
        userRepository.save(userEntity);

        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setUsername(userEntity.getUsername());
        roleEntity.setRole("ROLE_USER");
        roleRepository.save(roleEntity);
    }

    public UserEntity findByUsername(String username) {
        return  userRepository.findByUsername(username);
    }
}
