package com.rgsu.security.services;

import com.rgsu.security.domains.RoleEntity;
import com.rgsu.security.domains.UserEntity;
import com.rgsu.security.RoleRepository;
import com.rgsu.security.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by home on 05/08/16.
 * Сервис для авторизации
 */
@Service
public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity userEntity = userRepository.findByUsername(username);
        Set<RoleEntity> roleEntity = roleRepository.findByUsername(username);

        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        for (RoleEntity role : roleEntity) {
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getRole()));
        }

        return  new User(userEntity.getUsername(), userEntity.getPassword(), grantedAuthorities);
    }
}

