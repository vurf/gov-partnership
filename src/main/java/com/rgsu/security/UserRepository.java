package com.rgsu.security;

import com.rgsu.security.domains.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by home on 05/08/16.
 * Репозиторий для работы с бд
 */
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    UserEntity findByUsername(String username);

}
