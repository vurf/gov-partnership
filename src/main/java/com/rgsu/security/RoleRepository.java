package com.rgsu.security;

import com.rgsu.security.domains.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

/**
 * Created by home on 05/08/16.
 * Репозиторий для работы с бд
 */
public interface RoleRepository extends JpaRepository<RoleEntity, Long> {

    Set<RoleEntity> findByUsername(String username);

}
