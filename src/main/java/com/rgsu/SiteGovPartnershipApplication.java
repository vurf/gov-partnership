package com.rgsu;

import com.rgsu.config.JpaConfig;
import com.rgsu.config.JpaSecurityConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//Запуск приложения

@SpringBootApplication
public class SiteGovPartnershipApplication {

	public static void main(String[] args) {
		SpringApplication.run(new Class<?>[]{
				SiteGovPartnershipApplication.class,
				JpaConfig.class,
				JpaSecurityConfig.class
		}, args);
	}
}
