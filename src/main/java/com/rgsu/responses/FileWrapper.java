package com.rgsu.responses;

import org.springframework.web.multipart.MultipartFile;

/**
 * Created by vurf on 05.12.2016.
 */
public class FileWrapper {

    private MultipartFile file;
    private String name;

    public FileWrapper(MultipartFile file, String name) {
        this.file = file;
        this.name = name;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
